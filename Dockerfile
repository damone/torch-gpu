
# 初始化镜像，可根据环境和需要进行调整
FROM pytorch/pytorch:1.9.1-cuda11.1-cudnn8-runtime

# 设置软件源为阿里云
ADD ./sources.list /etc/apt/sources.list

# 更新时间及时区
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y tzdata \
    && ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && dpkg-reconfigure --force noninteractive tzdata

# 初始化 conda
RUN conda init

# 添加测试文件到容器中，现在通过启动容器时进行目录挂载来实现，注释后，可不用挂载卷
# ADD ./example /root/example

# 工作空间
WORKDIR /root/example

# 清理安装包缓存
RUN apt-get clean