docker build -t myself/torch-example .
docker run -itd \
        --restart=always \
        -v `pwd`/example:/root/example \
        --gpus all \
        --name torch-example myself/torch-example /bin/bash 